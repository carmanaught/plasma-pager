# Plasma Pager

This is my own personal fork of the `org.kde.plasma.pager` code, modifying some of the appearance and functionality. Note that this plasmoid should work under both X11 and Wayland.

While this can be used to copy over the top of the normal pager, my preference is to enable this per user by putting a copy of the pager plasmoid folder into `~/.local/share/plasma/plasmoids/` (create the parent folders as needed). This will cause the plasmoid to be loaded instead of the system plasma pager.

**Note:** This branch is for KDE Plasma 6. For KDE Plasma 5 use the [plasma5](`../../tree/plasma5`) branch. Additionally, the current version uses `qdbus6` via an executable call to launch the Overview effect. For this to work, <u>you must have qdbus6 installed and the Overview effect must be enabled</u>.

This is an example of the visual changes (showing my preferred configuration):

![Plasma Pager with Tooltip and Urgent higlighting](https://i.imgur.com/H4G1WXH.png)



### Changes in this fork

The pager tooltips have been modified to highlight the "Urgent" window in the list of windows when there is a window that demands attention. Note that this is not configurable.

Also see screenshots further below showing configuration dialog changes.

#### Window outline configuration

There are a couple of options to configure how window outlines appear. The default is to always show window outlines in keeping with the default configuration of the plasmoid, but there are two additional options for showing window outlines:

* **On hover**: Window outlines are only shown when hovering over a virtual desktop (as per the screenshot above). This is a potentially useful compromise between always and never.
* **Never**: Window outlines are never shown.



#### Pager 'desktop' styling

This includes a few different elements.

##### Label/virtual desktop styling based on virtual desktop state

Using the default color schemes - where "Dark" is intended for a dark panel and "Light" is intended for a light panel - there are 4 key settings for label styling:

* **Style Empty desktop labels**: When no window is open on a virtual desktop, the virtual desktop label on the pager should to appear strongly faded with the color close to the panel.
* **Style Inactive desktop labels**: When a window is open on a virtual desktop but the the virtual desktop is not the current desktop, the virtual desktop label on the pager should appear partially faded with the color somewhere between the empty desktop label and active desktop label.
* **Style Urgent desktop labels**: When a window is "demanding attention", the desktop label is intended to draw attention to that "urgency" for the user to act on.
* **Highlight Urgent Windows**: This causes window outlines (if enabled) to utilize the "urgent" color styling on the windows that are "demanding attention".

##### Font size

The font size of the label can be configured.

##### Pager spacing

The spacing for the pager views can be adjusted. **Note**: This is a bit buggy if set to larger values and needs more work, as currently it increases spacing from the center outwards and doesn't increase the total width of the pager plasmoid, so it can cause the plasmoid to overflow into neighboring plasmoids.

##### Pager frames

There are options to toggle the virtual desktop "frames" (separate to the labels) that appear by default and are similar to the window outlines, but are a visual indicator of the virtual desktop itself. There are effectively three types of frames:

* **Active**: This indicates the current virtual desktop.
* **Inactive**: This indicates the inactive virtual desktops.
* **Hover**: This is the frame appearance used when hovering of a virtual desktop.



#### Behavior - Selecting current virtual desktop 

There is an additional option for "Selecting current virtual desktop" which shows the overview. This uses `qdbus6` to trigger the Overview effect.



#### Colors

The configuration dialog includes additional configuration to modify the colors used. Note that a previous version had dark/light theme detection without color configuration, however this has been removed and replaced with the current configuration.

As noted above, there is a "Dark" theme intended for a dark panel and a "Light" theme intended for a light panel along with a "Custom" theme, allowing a user to configure the colors as they prefer. **Note**: The "Dark" theme is the default, purely because I use a dark panel, as can be seen in the screenshot above.

The configurable colors are configured for text color usage and then those text colors have some additional impact on the window outlines and pager frames. The configurable colors are:

* **Normal Text**: This is used for the label of the "active" (current) desktop. This is also the color used for the various window outlines apart from "urgent" windows.
* **Hover Text**: This is used for the "hover" desktop - the desktop the mouse is over.
* **Urgent Text**: This is used for the "urgent" desktop as well as the pager tooltip text color for "urgent" windows listed in the tooltip. This is also used for the "urgent" window outline.
* **Inactive Text**: This is used for the "inactive" desktops - those not current or hovered.
* **Empty Text**: This is used for the "empty" desktops - those with no windows on them.



### Screenshots

These are the general settings, configured with defaults in line with the original plasmoid.

![General Pager Settings](https://i.imgur.com/xUernEa.png)

The color configuration showing the dark theme. These colors can't be changed via the buttons.

![Color Settings - Dark](https://i.imgur.com/XYhzlze.png)

The color configuration showing the light theme. These colors can't be changed via the buttons.

![Color Settings - Light](https://i.imgur.com/Ak9zXbB.png)

The color configuration for the custom theme, which defaults to dark theme colors. Note that the color buttons can be clicked on here to change these colors.

![Color Settings - Custom](https://i.imgur.com/vqtp7H0.png)

After changing one of the default custom colors, a "reset" button becomes active to reset the color back to the default.

![Color Settings - Custom (with reset button active after changing color)](https://i.imgur.com/RGHCKpj.png)


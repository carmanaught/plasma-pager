    /*
 *  SPDX-FileCopyrightText: 2013 David Edmundson <davidedmundson@kde.org>
 *  SPDX-FileCopyrightText: 2016 Eike Hein <hein@kde.org>
 *
 *  SPDX-License-Identifier: GPL-2.0-or-later
 */

import QtQuick 2.5
import QtQuick.Controls 2.5 as QQC2

import org.kde.kirigami 2.5 as Kirigami
import org.kde.plasma.plasmoid 2.0
import org.kde.kcmutils as KCM

KCM.SimpleKCM {
    readonly property bool isActivityPager: Plasmoid.pluginName === "org.kde.plasma.activitypager"

    property int cfg_displayedText
    property alias cfg_showWindowIcons: showWindowIcons.checked
    property int cfg_currentDesktopSelected
    property int cfg_showWindowOutlines
    property alias cfg_pagerLayout: pagerLayout.currentIndex
    property alias cfg_showOnlyCurrentScreen: showOnlyCurrentScreen.checked
    property alias cfg_wrapPage: wrapPage.checked
    property alias cfg_styleEmptyDesktopLabels: styleEmptyDesktopLabels.checked
    property alias cfg_styleInactiveDesktopLabels: styleInactiveDesktopLabels.checked
    property alias cfg_styleUrgentDesktopLabels: styleUrgentDesktopLabels.checked
    property alias cfg_highlightUrgentWindows: highlightUrgentWindows.checked
    property alias cfg_labelFontPixelSize: labelFontPixelSize.value
    property alias cfg_pagerSpacingMultiplier: pagerSpacingMultiplier.value
    property alias cfg_showActiveDesktopFrame: showActiveDesktopFrame.checked
    property alias cfg_showInactiveDesktopFrame: showInactiveDesktopFrame.checked
    property alias cfg_showHoverFrame: showHoverFrame.checked

    Kirigami.FormLayout {
        QQC2.ButtonGroup {
            id: windowOutlineGroup
        }

        QQC2.ButtonGroup {
            id: displayedTextGroup
        }

        QQC2.ButtonGroup {
            id: currentDesktopSelectedGroup
        }


        QQC2.CheckBox {
            id: showWindowIcons

            Kirigami.FormData.label: i18n("General:")

            text: i18n("Show application icons on window outlines")
        }

        QQC2.CheckBox {
            id: showOnlyCurrentScreen
            text: i18n("Show only current screen")
        }

        QQC2.CheckBox {
            id: wrapPage
            text: i18n("Navigation wraps around")
        }

        Item {
            Kirigami.FormData.isSection: true
        }

        QQC2.RadioButton {
            id: alwaysWindowOutline

            Kirigami.FormData.label: i18n("Show window outline:")

            QQC2.ButtonGroup.group: windowOutlineGroup
            text: i18n("Always")
            checked: cfg_showWindowOutlines === 0
            onToggled: if (checked) cfg_showWindowOutlines = 0;
        }

        QQC2.RadioButton {
            id: hoverWindowOutline
            QQC2.ButtonGroup.group: windowOutlineGroup
            text: i18n("On hover")
            checked: cfg_showWindowOutlines === 1
            onToggled: if (checked) cfg_showWindowOutlines = 1;
        }

        QQC2.RadioButton {
            id: noWindowOutline
            QQC2.ButtonGroup.group: windowOutlineGroup
            text: i18n("Never")
            checked: cfg_showWindowOutlines === 2
            onToggled: if (checked) cfg_showWindowOutlines = 2;
        }

        Item {
            Kirigami.FormData.isSection: true
        }

        QQC2.CheckBox {
            id: styleEmptyDesktopLabels

            Kirigami.FormData.label: i18n("Styling:")

            text: i18n("Style Empty desktop labels")
        }

        QQC2.CheckBox {
            id: styleInactiveDesktopLabels
            text: i18n("Style Inactive desktop labels")
        }

        QQC2.CheckBox {
            id: styleUrgentDesktopLabels
            text: i18n("Style Urgent desktop labels")
        }

        QQC2.CheckBox {
            id: highlightUrgentWindows
            text: i18n("Highlight Urgent Windows")
        }

        QQC2.SpinBox {
            id: labelFontPixelSize
            Kirigami.FormData.label: i18n("Font pixel size (Theme default: " + Kirigami.Theme.defaultFont.pixelSize + "):")
            value: plasmoid.configuration.labelFontPixelSize
            from: 1
            stepSize: 1
        }

        QQC2.SpinBox {
            id: pagerSpacingMultiplier
            Kirigami.FormData.label: i18n("Pager spacing multiplier:")
            value: plasmoid.configuration.pagerSpacingMultiplier
            from: 1
            stepSize: 1
        }

        Item {
            Kirigami.FormData.isSection: true
        }

        QQC2.CheckBox {
            id: showActiveDesktopFrame

            Kirigami.FormData.label: i18n("Pager Frames:")

            text: i18n("Show active desktop frame")
        }

        QQC2.CheckBox {
            id: showInactiveDesktopFrame
            text: i18n("Show inactive desktop frame")
        }

        QQC2.CheckBox {
            id: showHoverFrame
            text: i18n("Show mouse hover frame")
        }

        Item {
            Kirigami.FormData.isSection: true
        }


        QQC2.ComboBox {
            id: pagerLayout

            Kirigami.FormData.label: i18n("Layout:")

            model: [i18nc("The pager layout", "Default"), i18n("Horizontal"), i18n("Vertical")]
            visible: isActivityPager
        }


        Item {
            Kirigami.FormData.isSection: true
            visible: isActivityPager
        }


        QQC2.RadioButton {
            id: noTextRadio

            Kirigami.FormData.label: i18n("Text display:")

            QQC2.ButtonGroup.group: displayedTextGroup
            text: i18n("No text")
            checked: cfg_displayedText === 2
            onToggled: if (checked) cfg_displayedText = 2;
        }

        QQC2.RadioButton {
            id: desktopNumberRadio
            QQC2.ButtonGroup.group: displayedTextGroup
            text: isActivityPager ? i18n("Activity number") : i18n("Desktop number")
            checked: cfg_displayedText === 0
            onToggled: if (checked) cfg_displayedText = 0;
        }

        QQC2.RadioButton {
            id: desktopNameRadio
            QQC2.ButtonGroup.group: displayedTextGroup
            text: isActivityPager ? i18n("Activity name") : i18n("Desktop name")
            checked: cfg_displayedText === 1
            onToggled: if (checked) cfg_displayedText = 1;
        }


        Item {
            Kirigami.FormData.isSection: true
        }


        QQC2.RadioButton {
            id: doesNothingRadio

            Kirigami.FormData.label: isActivityPager ? i18n("Selecting current Activity:") : i18n("Selecting current virtual desktop:")

            QQC2.ButtonGroup.group: currentDesktopSelectedGroup
            text: i18n("Does nothing")
            checked: cfg_currentDesktopSelected === 0
            onToggled: if (checked) cfg_currentDesktopSelected = 0;
        }

        QQC2.RadioButton {
            id: showsDesktopRadio
            QQC2.ButtonGroup.group: currentDesktopSelectedGroup
            text: i18n("Shows the desktop")
            checked: cfg_currentDesktopSelected === 1
            onToggled: if (checked) cfg_currentDesktopSelected = 1;
        }

        QQC2.RadioButton {
            id: showsOverviewRadio
            QQC2.ButtonGroup.group: currentDesktopSelectedGroup
            text: i18n("Shows the overview")
            checked: cfg_currentDesktopSelected === 2
            onToggled: if (checked) cfg_currentDesktopSelected = 2;
        }
    }
}

    /*
 *  SPDX-FileCopyrightText: 2013 David Edmundson <davidedmundson@kde.org>
 *  SPDX-FileCopyrightText: 2016 Eike Hein <hein@kde.org>
 *
 *  SPDX-License-Identifier: GPL-2.0-or-later
 */

import QtQuick 2.5
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Layouts 1.1

import org.kde.kirigami 2.5 as Kirigami
import org.kde.plasma.plasmoid 2.0
import org.kde.kcmutils as KCM

KCM.SimpleKCM {
    // Pre-defined colors for different "themes"
    readonly property color darkNormalText: "#fcfcfc"
    readonly property color darkHoverText: "#fcfcfc"
    readonly property color darkUrgentText: "#da4453"
    readonly property color darkInactiveText: "#656565"
    readonly property color darkEmptyText: "#262626"
    readonly property color lightNormalText: "#232627"
    readonly property color lightHoverText: "#232627"
    readonly property color lightUrgentText: "#da4453"
    readonly property color lightInactiveText: "#7F8C8d"
    readonly property color lightEmptyText: "#cdcdcd"

    // Preferences for the currently set colors and the custom colors
    property alias cfg_normalText: btnNormalText.color
    property alias cfg_hoverText: btnHoverText.color
    property alias cfg_urgentText: btnUrgentText.color
    property alias cfg_inactiveText: btnInactiveText.color
    property alias cfg_emptyText: btnEmptyText.color
    property color cfg_customNormalText
    property color cfg_customHoverText
    property color cfg_customUrgentText
    property color cfg_customInactiveText
    property color cfg_customEmptyText
    property int cfg_currentTheme

    Kirigami.FormLayout {
        QQC2.ButtonGroup { id: colorSchemes }
        RowLayout {
            id: colorSchemeButtons
            spacing: 0

            QQC2.Button {
                id: dark
                text: i18n("Dark")
                checkable: true

                QQC2.ButtonGroup.group: colorSchemes
                checked: cfg_currentTheme == 0
                onToggled: if (checked) {
                    cfg_currentTheme = 0;

                    btnNormalText.color = darkNormalText;
                    btnHoverText.color = darkHoverText;
                    btnUrgentText.color = darkUrgentText;
                    btnInactiveText.color = darkInactiveText;
                    btnEmptyText.color = darkEmptyText;
                }
            }

            QQC2.Button {
                id: light
                text: i18n("Light")
                checkable: true

                QQC2.ButtonGroup.group: colorSchemes
                checked: cfg_currentTheme == 1
                onToggled: if (checked) {
                    cfg_currentTheme = 1;

                    btnNormalText.color = lightNormalText;
                    btnHoverText.color = lightHoverText;
                    btnUrgentText.color = lightUrgentText;
                    btnInactiveText.color = lightInactiveText;
                    btnEmptyText.color = lightEmptyText;
                }
            }

            QQC2.Button {
                id: custom
                text: i18n("Custom")
                checkable: true

                QQC2.ButtonGroup.group: colorSchemes
                checked: cfg_currentTheme == 2
                onToggled: if (checked) {
                    cfg_currentTheme = 2;
                    btnNormalText.color = cfg_customNormalText;
                    btnHoverText.color = cfg_customHoverText;
                    btnUrgentText.color = cfg_customUrgentText;
                    btnInactiveText.color = cfg_customInactiveText;
                    btnEmptyText.color = cfg_customEmptyText;

                }
            }
        }

        Item {
            Kirigami.FormData.isSection: true
        }

        RowLayout {
            id: normalText
            Kirigami.FormData.label: i18n("Normal Text:")
            ColorButton {
                id: btnNormalText
                showAlphaChannel: false
                dialogTitle: i18n("Normal Text")
                toolTipText: i18n("This is the color for the currently active desktop.") +
                                i18n("This will also affect the window outlines.") +
                                "\n\n" + i18n("The color is ") + btnNormalText.color + "."
                color: {
                    if (cfg_currentTheme == 0) { darkNormalText; }
                    else if (cfg_currentTheme == 1) { lightNormalText; }
                    else if (cfg_currentTheme == 2) { cfg_customNormalText; }
                }
                enabled: cfg_currentTheme == 2
                visible: true
                onAccepted: {
                    cfg_normalText = color;
                    cfg_customNormalText = color;
                }
            }

            QQC2.Button {
                icon.name: "edit-reset"
                enabled: ((cfg_customNormalText != darkNormalText) && cfg_currentTheme == 2)
                visible: cfg_currentTheme == 2
                Layout.preferredHeight: btnNormalText.height
                onClicked: {
                    cfg_customNormalText = darkNormalText;
                    btnNormalText.color = cfg_customNormalText;
                }
            }
        }

        RowLayout {
            id: hoverText
            Kirigami.FormData.label: i18n("Hover Text:")
            ColorButton {
                id: btnHoverText
                showAlphaChannel: false
                dialogTitle: i18n("Hover Text")
                toolTipText: i18n("This is the color for the desktop being hovered over.") +
                                    "\n\n" + i18n("The color is ") + btnHoverText.color + "."
                color: {
                    if (cfg_currentTheme == 0) { darkHoverText; }
                    else if (cfg_currentTheme == 1) { lightHoverText; }
                    else if (cfg_currentTheme == 2) { cfg_customHoverText; }
                }
                enabled: cfg_currentTheme == 2
                visible: true
                onAccepted: {
                    cfg_hoverText = color;
                    cfg_customHoverText = color;
                }
            }

            QQC2.Button {
                icon.name: "edit-reset"
                enabled: ((cfg_customHoverText != darkHoverText) && cfg_currentTheme == 2)
                visible: cfg_currentTheme == 2
                Layout.preferredHeight: btnHoverText.height
                onClicked: {
                    cfg_customHoverText = darkHoverText;
                    btnHoverText.color = cfg_customHoverText;
                }
            }
        }

        RowLayout {
            id: urgentText
            Kirigami.FormData.label: i18n("Urgent Text:")
            ColorButton {
                id: btnUrgentText
                showAlphaChannel: false
                dialogTitle: i18n("Urgent Text")
                toolTipText: i18n("This is the color for a desktop with an urgent window.") +
                                    "\n\n" + i18n("The color is ") + btnUrgentText.color + "."
                color: {
                    if (cfg_currentTheme == 0) { darkUrgentText; }
                    else if (cfg_currentTheme == 1) { lightUrgentText; }
                    else if (cfg_currentTheme == 2) { cfg_customUrgentText; }
                }
                enabled: cfg_currentTheme == 2
                visible: true
                onAccepted: {
                    cfg_urgentText = color;
                    cfg_customUrgentText = color;
                }
            }

            QQC2.Button {
                icon.name: "edit-reset"
                enabled: ((cfg_customUrgentText != darkUrgentText) && cfg_currentTheme == 2)
                visible: cfg_currentTheme == 2
                Layout.preferredHeight: btnUrgentText.height
                onClicked: {
                    cfg_customUrgentText = darkUrgentText;
                    btnUrgentText.color = cfg_customUrgentText;
                }
            }
        }

        RowLayout {
            id: inactiveText
            Kirigami.FormData.label: i18n("Inactive Text:")
            ColorButton {
                id: btnInactiveText
                showAlphaChannel: false
                dialogTitle: i18n("Inactive Text")
                toolTipText: i18n("This is the color for a desktop WITH applications ") +
                                    i18n("open that isn't the active desktop.") +
                                    "\n\n" + i18n("The color is ") + btnInactiveText.color + "."
                color: {
                    if (cfg_currentTheme == 0) { darkInactiveText; }
                    else if (cfg_currentTheme == 1) { lightInactiveText; }
                    else if (cfg_currentTheme == 2) { cfg_customInactiveText; }
                }
                enabled: cfg_currentTheme == 2
                visible: true
                onAccepted: {
                    cfg_inactiveText = color;
                    cfg_customInactiveText = color;
                }
            }

            QQC2.Button {
                icon.name: "edit-reset"
                enabled: ((cfg_customInactiveText != darkInactiveText) && cfg_currentTheme == 2)
                visible: cfg_currentTheme == 2
                Layout.preferredHeight: btnInactiveText.height
                onClicked: {
                    cfg_customInactiveText = darkInactiveText;
                    btnInactiveText.color = cfg_customInactiveText;
                }
            }
        }

        RowLayout {
            id: emptyText
            Kirigami.FormData.label: i18n("Empty Text:")
            ColorButton {
                id: btnEmptyText
                showAlphaChannel: false
                dialogTitle: i18n("Empty Text")
                toolTipText: i18n("This is the color for a desktop WITHOUT applications ") +
                                    i18n("open that isn't the active desktop.") +
                                    "\n\n" + i18n("The color is ") + btnEmptyText.color + "."
                color: {
                    if (cfg_currentTheme == 0) { darkEmptyText; }
                    else if (cfg_currentTheme == 1) { lightEmptyText; }
                    else if (cfg_currentTheme == 2) { cfg_customEmptyText; }
                }
                enabled: cfg_currentTheme == 2
                visible: true
                onAccepted: {
                    cfg_emptyText = color;
                    cfg_customEmptyText = color;
                }
            }

            QQC2.Button {
                icon.name: "edit-reset"
                enabled: ((cfg_customEmptyText != darkEmptyText) && cfg_currentTheme == 2)
                visible: cfg_currentTheme == 2
                Layout.preferredHeight: btnEmptyText.height
                onClicked: {
                    cfg_customEmptyText = darkEmptyText;
                    btnEmptyText.color = cfg_customEmptyText;
                }
            }
        }
    }
}

import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs
import QtQuick.Layouts

import org.kde.kirigami as Kirigami

/* Based on ColorButton.qml by luisbocanegra for the Plasma Panel Colorizer plasmoid:
 * https://github.com/luisbocanegra/plasma-panel-colorizer
 */

Button {
    id: root

    property color color // The user selected color
    property string dialogTitle // Title to show in the dialog
    property string toolTipText // Text to show for button tooltip
    property bool showAlphaChannel: false // Allow the user to configure an alpha value

    // This signal is emitted when the color dialog has been accepted @since 5.61
    signal accepted(color color)

    contentItem: Item {
        implicitWidth: btnLayout.implicitWidth
        implicitHeight: btnLayout.implicitHeight
        RowLayout {
            id: btnLayout
            // FIXME: Using 'anchors.centerIn: parent' seems to anchor the top-left of the
            // rectangle. Will have to understand how this works better and fix this.
            anchors {
                top: parent.top
                left: parent.left
                leftMargin: 6
                topMargin: 6
            }

            Rectangle {
                color: root.color
                height: root.height - 12
                width: root.width - 12
                border {
                    width: 1
                    color: Kirigami.Theme.disabledTextColor
                }
            }
        }
    }

    hoverEnabled: true
    ToolTip.delay: 500
    ToolTip.timeout: 5000
    ToolTip.visible: hovered
    ToolTip.text: root.toolTipText

    Component {
        id: colorWindowComponent

        Window { // QTBUG-119055 https://invent.kde.org/plasma/kdeplasma-addons/-/commit/797cef06882acdf4257d8c90b8768a74fdef0955
            id: window
            width: Kirigami.Units.gridUnit * 22
            height: Kirigami.Units.gridUnit * 24
            visible: true
            title: root.dialogTitle
            ColorDialog {
                id: colorDialog
                //title: root.dialogTitle
                selectedColor: root.color || undefined // Prevent transparent colors
                options: root.showAlphaChannel
                parentWindow: window.Window.window
                onAccepted: {
                    root.color = selectedColor
                    root.accepted(selectedColor);
                    window.destroy();
                }
                onRejected: window.destroy()
            }
            onClosing: destroy()
            Component.onCompleted: colorDialog.open()
        }
    }

    onClicked: {
        colorWindowComponent.createObject(root)
    }
}